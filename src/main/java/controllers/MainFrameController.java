package controllers;

import classification.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import model.Article;
import model.Results;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MainFrameController {

    private List<Article> articles;
    private List<Integer> attributes;
    private int trainingAmount = 20;
    private int k;
    private String[] countries = {"usa", "japan", "uk", "france", "west-germany", "canada"};

    @FXML
    private Pane mainFramePane;
    @FXML
    private TextField loadDirectoryPath;
    @FXML
    private ComboBox metricChooser;
    @FXML
    private TextField kNeighboursAmount;
    @FXML
    private Slider trainingPercentageAmountSlider;
    @FXML
    private TextField trainingPercentageAmountLabel;
    @FXML
    private CheckBox chB1ShortWords;
    @FXML
    private CheckBox chB2MediumWords;
    @FXML
    private CheckBox chB3LongWords;
    @FXML
    private CheckBox chB4MetricUnits;
    @FXML
    private CheckBox chB5UpperCaseWords;
    @FXML
    private CheckBox chB6MostCommonFromRarely;
    @FXML
    private CheckBox chB7PunctuationMarks;
    @FXML
    private CheckBox chB8DigitsInText;
    @FXML
    private CheckBox chB9CheckCurrency;
    @FXML
    private CheckBox chB10DistinctWords;

    @FXML
    private Button computeBtn;
    @FXML
    private TableColumn originalCountriesCol;
    @FXML
    private TableColumn usaCol;
    @FXML
    private TableColumn japanCol;
    @FXML
    private TableColumn franceCol;
    @FXML
    private TableColumn canadaCol;
    @FXML
    private TableColumn ukCol;
    @FXML
    private TableColumn westGermanyCol;
    @FXML
    private TableColumn placesCol;
    @FXML
    private TableColumn precisionCol;
    @FXML
    private TableColumn recallCol;
    @FXML
    private TableColumn f1Col;
    @FXML
    private TableView mistakeTable;
    @FXML
    private TableView resultsTable;
    @FXML
    private TextField accuracyField;
    @FXML
    private TextField precisionField;
    @FXML
    private TextField recallField;
    @FXML
    private TextField f1Field;



    public void initialize() {
        loadDirectoryPath.setDisable(true);
        trainingPercentageAmountLabel.setDisable(true);

        metricChooser.getItems().addAll("Euclidean", "Manhattan", "Chebyshev");
        trainingPercentageAmountLabel.setText(trainingAmount + "");

        trainingPercentageAmountSlider.setMin(1);
        trainingPercentageAmountSlider.setMax(99);
        trainingPercentageAmountSlider.setValue(20);
        trainingPercentageAmountSlider.setMajorTickUnit(1);
        trainingPercentageAmountSlider.setSnapToTicks(true);
        trainingPercentageAmountSlider.setMinorTickCount(0);
        trainingPercentageAmountSlider.setBlockIncrement(1);

        trainingPercentageAmountSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            trainingPercentageAmountSlider.setValue(newValue.intValue());
            trainingAmount = (int) trainingPercentageAmountSlider.getValue();
            trainingPercentageAmountLabel.setText(trainingAmount + "");
        });

        chB1ShortWords.setSelected(true);
        chB2MediumWords.setSelected(true);
        chB3LongWords.setSelected(true);
        chB4MetricUnits.setSelected(true);
        chB5UpperCaseWords.setSelected(true);
        chB6MostCommonFromRarely.setSelected(true);
        chB7PunctuationMarks.setSelected(true);
        chB8DigitsInText.setSelected(true);
        chB9CheckCurrency.setSelected(true);
        chB10DistinctWords.setSelected(true);


        originalCountriesCol.setCellValueFactory(new PropertyValueFactory<MistakeArrayRow, String>("country"));
        usaCol.setCellValueFactory(new PropertyValueFactory<MistakeArrayRow, String>("country1"));
        japanCol.setCellValueFactory(new PropertyValueFactory<MistakeArrayRow, String>("country2"));
        ukCol.setCellValueFactory(new PropertyValueFactory<MistakeArrayRow, String>("country3"));
        franceCol.setCellValueFactory(new PropertyValueFactory<MistakeArrayRow, String>("country4"));
        westGermanyCol.setCellValueFactory(new PropertyValueFactory<MistakeArrayRow, String>("country5"));
        canadaCol.setCellValueFactory(new PropertyValueFactory<MistakeArrayRow, String>("country6"));

        placesCol.setCellValueFactory(new PropertyValueFactory<ResultsArrayRow, String>("country"));
        precisionCol.setCellValueFactory(new PropertyValueFactory<ResultsArrayRow, String>("precision"));
        recallCol.setCellValueFactory(new PropertyValueFactory<ResultsArrayRow, String>("recall"));
        f1Col.setCellValueFactory(new PropertyValueFactory<ResultsArrayRow, String>("f1"));

    }

    public void loadDirectory() {
        try {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setInitialDirectory(new File(System.getProperty("user.home") + "\\Desktop"));

            File file = directoryChooser.showDialog(mainFramePane.getScene().getWindow());
            articles = new model.OurFileReader().readAllFiles(file.getAbsolutePath());
            loadDirectoryPath.setText(file.getAbsolutePath());

        } catch (NullPointerException e) {

        }
    }

    public void compute() {
        System.out.println("uruchomienie obliczen");
        resultsTable.getItems().clear();
        mistakeTable.getItems().clear();
        computeBtn.setDisable(true);

        Metric metric = new Euclidean(attributes);
        int chosen = metricChooser.getSelectionModel().getSelectedIndex();
        k = Integer.parseInt(kNeighboursAmount.getText());
        readCheckboxes();
        articles.forEach(article -> article.extractArticle(attributes));
        switch (chosen) {
            case 0 -> {
                metric = new Euclidean(attributes);

            }
            case 1 -> {
                metric = new Manhattan(attributes);
            }
            case 2 -> {
                metric = new Chebyshev(attributes);
            }
        }

        Knn knn = new Knn(metric, articles, attributes, k);
        knn.splitIntoLearningAndTrainingGroup(trainingAmount);
        List<Article> articlesAfterClassification = knn.classification().stream().filter(x->!x.isLearning()).collect(Collectors.toList());

        calculateResults(articlesAfterClassification);
        fillResultsTable(articlesAfterClassification);
        calculateRates(articlesAfterClassification);

        computeBtn.setDisable(false);
    }

    public void readCheckboxes() {
        attributes = new ArrayList<>();
        if (chB1ShortWords.isSelected()) attributes.add(1);
        if (chB2MediumWords.isSelected()) attributes.add(2);
        if (chB3LongWords.isSelected()) attributes.add(3);
        if (chB4MetricUnits.isSelected()) attributes.add(4);
        if (chB5UpperCaseWords.isSelected()) attributes.add(5);
        if (chB6MostCommonFromRarely.isSelected()) attributes.add(6);
        if (chB7PunctuationMarks.isSelected()) attributes.add(7);
        if (chB8DigitsInText.isSelected()) attributes.add(8);
        if (chB9CheckCurrency.isSelected()) attributes.add(9);
        if (chB10DistinctWords.isSelected()) attributes.add(10);
    }

    public void calculateResults(List<Article> articlesAfterClassification) {
        Results results = new Results(articlesAfterClassification);
        int[][] mistakeArray = results.mistakeTable();
        ObservableList<MistakeArrayRow> row = FXCollections.<MistakeArrayRow>observableArrayList();

        for (int i = 0; i < mistakeArray.length; i++) {

            row.add(new MistakeArrayRow(countries[i], String.valueOf(mistakeArray[i][0]),
                    String.valueOf(mistakeArray[i][1]),
                    String.valueOf(mistakeArray[i][2]),
                    String.valueOf(mistakeArray[i][3]),
                    String.valueOf(mistakeArray[i][4]),
                    String.valueOf(mistakeArray[i][5])
            ));

        }
        mistakeTable.getItems().addAll(row);

    }

    public void fillResultsTable(List<Article> articlesAfterClassification) {
        Results results = new Results(articlesAfterClassification);
        ObservableList<ResultsArrayRow> row = FXCollections.<ResultsArrayRow>observableArrayList();

        for (int i = 0; i < 6; i++){
            row.add(new ResultsArrayRow(countries[i],
                                        String.valueOf(results.calcPrecisionForOneCountry(countries[i])),
                                        String.valueOf(results.calcRecallForOneCountry(countries[i])),
                                        String.valueOf(results.calcF1ForOneCountry(countries[i]))));
        }
        resultsTable.getItems().addAll(row);

    }

    private void calculateRates(List<Article> articlesAfterClassification){
        Results results = new Results(articlesAfterClassification);
        accuracyField.setText(String.valueOf(results.calculateAccuracy()));
        precisionField.setText(String.valueOf(results.calculatePrecision()));
        recallField.setText(String.valueOf(results.calculateRecall()));
        f1Field.setText(String.valueOf(results.calculateF1()));
    }

    public class MistakeArrayRow {
        public String country;
        public String country1;
        private final String country2;
        private final String country3;
        private final String country4;
        private final String country5;
        private final String country6;

        public String getCountry() {
            return country;
        }

        public String getCountry1() {
            return country1;
        }

        public String getCountry2() {
            return country2;
        }

        public String getCountry3() {
            return country3;
        }

        public String getCountry4() {
            return country4;
        }

        public String getCountry5() {
            return country5;
        }

        public String getCountry6() {
            return country6;
        }


        public MistakeArrayRow(String country, String country1, String country2, String country3, String country4, String country5, String country6) {
            this.country = (country);
            this.country1 = (country1);
            this.country2 = (country2);
            this.country3 = (country3);
            this.country4 = (country4);
            this.country5 = (country5);
            this.country6 = (country6);
        }
    }

    public class ResultsArrayRow{
        private final String country;
        private final String recall;
        private final String precision;
        private final String f1;

        public ResultsArrayRow(String country, String recall, String precision, String f1) {
            this.country = country;
            this.recall = recall;
            this.precision = precision;
            this.f1 = f1;
        }

        public String getCountry() {
            return country;
        }

        public String getRecall() {
            return recall;
        }

        public String getPrecision() {
            return precision;
        }

        public String getF1() {
            return f1;
        }
    }

}

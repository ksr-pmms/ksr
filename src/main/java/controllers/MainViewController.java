package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

import java.io.IOException;

/**
 * @author Patryk Markowski
 */
public class MainViewController {

    @FXML
    private Pane mainViewPane;

    @FXML
    public void initialize() {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/MainFrame.fxml"));
        Pane mainFramePane = null;
        try {
            mainFramePane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainViewPane.getChildren().add(mainFramePane);
    }

}

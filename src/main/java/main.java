import classification.*;
import extraction.AttributesExtraction;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Article;
import model.Results;

import java.util.Arrays;
import java.util.List;


/**
 * @author Patryk Markowski
 */
public class main extends Application {

    public static void main(String[] args) {
        launch(args);

/*
        List<Article> articles = model.OurFileReader.readAllFiles("C:\\Users\\Patryk\\Desktop\\articles");
        List<Integer> attr= Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);


        articles.forEach(article -> article.extractArticle(attr));

        Knn knn = new Knn(new Euclidean(attr),articles,attr, 5);

        knn.splitIntoLearningAndTrainingGroup(50);
        List<Article>after =knn.classification();
        System.out.println("Całosc "+after.size() );
        int good=0;
        int notGood=0;
//        for(Article article :after){
//            if(article.getClassifiedPlace().equals(article.getPlace())){
//                good++;
//            }
//            else{
//                notGood++;
//            }
//        }
//        System.out.println("Good "+good);
//        System.out.println("NotGood "+notGood);
//        wypisz("west-germany",after);
//        wypisz("usa",after);
//        wypisz("france",after);
//        wypisz("uk",after);
//        wypisz("canada",after);
//        wypisz("japan",after);
//        splitByCountry("west-germany");
//        splitByCountry("usa");
//        splitByCountry("france");
//        splitByCountry("uk");
//        splitByCountry("canada");
//        splitByCountry("japan");
        Results results = new Results(after);
        int[][] arr = results.mistakeTable();
        for(int i=0;i<6;i++){
            String res="";
            for(int j=0;j<6;j++){
                res+=arr[i][j]+" ";
            }
            System.out.println(res);
        }


//        Metric metric = new Manhattan(attributes);
//        System.out.println(metric.compareString("YYYYYYYYYY","PROGRAMMING"));
    }
//    public static void wypisz (String country,List<Article>after){
//        System.out.println(country);
//        System.out.println("Powinno być "+ after.stream().filter(article -> article.getPlace().equals(country)).count());
//        Results results = new Results(after);
//
//        List<Integer> res = results.calc(country);
//        res.forEach(System.out::println);
//        System.out.println();
//    }
//
 */
    }

    @Override
    public void start(Stage stage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/MainView.fxml"));
        Pane mainPane = loader.load();
        Scene scene = new Scene(mainPane);

        stage.setScene(scene);
        stage.setTitle("KSR");
        stage.show();
    }

}

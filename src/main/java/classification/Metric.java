package classification;

import model.Article;

/**
 * @author Patryk Markowski
 */
public interface Metric {
    double getDistance(Article first, Article second);

    //ngram
    default double compareString(String first, String second) {
        int similarity = 0;
        int n = 3;
        int N = 0;
        if(first.length()<3 || second.length()<3) n=1;
        if (first.length() > second.length()) {
            N = first.length();
            for (int i = 0; i < second.length() - (n-1); i++) {
                String tmp = second.substring(i, i + (n));
                if (first.contains(tmp)) similarity++;
            }
        } else {
            N = second.length();
            for (int i = 0; i < first.length() - (n-1); i++) {
                String tmp = first.substring(i, i + (n));
                if (second.contains(tmp)) {
                    similarity++;
                }

            }

        }

        return (double)similarity / (N - n + 1);
    }
}

package classification;

import model.Article;

import java.util.List;

/**
 * @author Patryk Markowski
 */
public class Euclidean implements Metric{
    private final List<Integer> attributes;
    public Euclidean(List<Integer> attributes) {
        this.attributes = attributes;
    }

    @Override
    public double getDistance(Article first,Article second) {
        double sum=0.0;

        for(int i=0;i<first.getAttributes().size();i++){
            sum+= Math.pow(first.getAttributes().get(i)-second.getAttributes().get(i),2);
        }

        //TODO czy to też ma być do potengi?????????
        if(attributes.contains(4)){
            sum+=Math.pow(compareString(first.getMetric(),second.getMetric()),2);

        }
        if(attributes.contains(6)){
            sum+=Math.pow(compareString(first.getMostCommonWordFromRarely(),second.getMostCommonWordFromRarely()),2);
        }
        if(attributes.contains(9)){
            sum+= Math.pow(compareString(first.getCurrency(),second.getCurrency()),2);

        }
        return Math.sqrt(sum);
    }

}

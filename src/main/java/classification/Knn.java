package classification;

import model.Article;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.*;

/**
 * @author Patryk Markowski
 */
public class Knn {

    private final Metric metric;
    private final List<Article> extractedArticle;
    private List<Article> searchSpace = new ArrayList<>();
    private List<Article> testGroup = new ArrayList<>();

    private final List<Integer> attributes;
    private final Integer k;

    public Knn(Metric metric, List<Article> extractedArticle, List<Integer> attributes, Integer k) {
        this.metric = metric;
        this.extractedArticle = extractedArticle;
        this.attributes = attributes;
        this.k = k;
    }

    public void splitIntoLearningAndTrainingGroup(Integer trainingGroupPercentage) {

        splitByCountry("west-germany", trainingGroupPercentage);
        splitByCountry("usa", trainingGroupPercentage);
        splitByCountry("france", trainingGroupPercentage);
        splitByCountry("uk", trainingGroupPercentage);
        splitByCountry("canada", trainingGroupPercentage);
        splitByCountry("japan", trainingGroupPercentage);

        for (Article article : searchSpace) {
            article.setClassifiedPlace(article.getPlace());
        }
        shuffle(searchSpace);
        shuffle(testGroup);


    }

    private void splitByCountry(String country, Integer trainingGroupPercentage) {
        List<Article> articlesForCountry = extractedArticle.stream()
                .filter(article -> article.getPlace().equals(country))
                .collect(Collectors.toList());

        int iterationLimitForLearning = (int) trainingGroupPercentage * articlesForCountry.size() / 100;

        for (int i = 0; i < articlesForCountry.size(); i++) {
            if (i < iterationLimitForLearning) {
                articlesForCountry.get(i).setLearning(true);
                searchSpace.add(articlesForCountry.get(i));
            } else {
                testGroup.add(articlesForCountry.get(i));
            }
        }
    }

    public List<Article> classification() {
        for (Article article : testGroup) {
            classify(article);
//            article.setClassifiedPlace(classify(article));
        }
        return testGroup;
    }

    private void classify(Article singleArticle) {
        List<distArtice> distArtices = new ArrayList<>();

        for (Article article : searchSpace) {
            distArtices.add(new distArtice(article, metric.getDistance(article, singleArticle)));
        }

        distArtices.sort(new Comparator<distArtice>() {
            @Override
            public int compare(distArtice o1, distArtice o2) {
                return o1.getDistance().compareTo(o2.getDistance());
            }
        });

        List<distArtice> nearestNeighbours = distArtices.stream().limit(k).collect(Collectors.toList());
        //wybrać najczęścięj występujące  i przypisać do singleArticle

        singleArticle.setClassifiedPlace(mostCommonInNearestNeighbours(nearestNeighbours));
        searchSpace.add(singleArticle);
//        return mostCommonInNearestNeighbours(nearestNeighbours);
    }

    private String mostCommonInNearestNeighbours(List<distArtice> nearestArticles){
        String mostCommon = "";
        Map<String,Integer> map = new HashMap<>();
        for(distArtice article: nearestArticles){
            if(map.containsKey(article.getArticle().getClassifiedPlace())){

                map.put(article.getArticle().getClassifiedPlace(),map.get(article.getArticle().getClassifiedPlace())+1);
            }
            else{
                map.put(article.getArticle().getClassifiedPlace(),1);
            }
        }
        Map.Entry<String,Integer> maxEntry = max(map.entrySet(), new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o1.getValue()
                        .compareTo(o2.getValue());
            }
        });
        return maxEntry.getKey();
    }



    class distArtice {
        public Article article;
        public Double distance;

        public distArtice(Article article, double distance) {
            this.article = article;
            this.distance = distance;
        }

        public Article getArticle() {
            return article;
        }

        public Double getDistance() {
            return distance;
        }

    }
}

package classification;

import model.Article;

import java.util.List;

/**
 * @author Patryk Markowski
 */
public class Manhattan implements Metric{

    private final List<Integer> attributes;
    public Manhattan(List<Integer> attributes) {
        this.attributes = attributes;
    }

    @Override
    public double getDistance(Article first, Article second) {
        double sum=0.0;

        for(int i=0;i<first.getAttributes().size();i++){
            sum+= Math.abs(first.getAttributes().get(i)-second.getAttributes().get(i));
        }
        if(attributes.contains(4)){
            sum+=Math.abs(compareString(first.getMetric(),second.getMetric()));

        }
        if(attributes.contains(6)){
            sum+=Math.abs(compareString(first.getMostCommonWordFromRarely(),second.getMostCommonWordFromRarely()));
        }
        if(attributes.contains(9)){
            sum+= Math.abs(compareString(first.getCurrency(),second.getCurrency()));

        }

        return sum;
    }
}

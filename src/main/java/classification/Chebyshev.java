package classification;

import model.Article;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Patryk Markowski
 */
public class Chebyshev implements Metric{

    private final List<Integer> attributes;
    public Chebyshev(List<Integer> attributes) {
        this.attributes = attributes;
    }

    @Override
    public double getDistance(Article first, Article second) {
        List<Double> differences =new ArrayList<>();


        for(int i=0;i<first.getAttributes().size();i++){
            differences.add((double) (first.getAttributes().get(i)-second.getAttributes().get(i)));
        }
        if(attributes.contains(4)){
            differences.add(compareString(first.getMetric(),second.getMetric()));

        }
        if(attributes.contains(6)){
            differences.add(compareString(first.getMostCommonWordFromRarely(),second.getMostCommonWordFromRarely()));
        }
        if(attributes.contains(9)){
            differences.add(compareString(first.getCurrency(),second.getCurrency()));

        }

        return differences.stream().max(Double::compare).get();
    }
}

package model;

import extraction.AttributesExtraction;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Patryk Markowski
 */
public class Article {
    public final String place;
    private final String text;


    private List<Integer> attributes=new ArrayList<>();
    private boolean isLearning=false;

    private String classifiedPlace = "";

    private String currency;
    private String metric;
    private String mostCommonWordFromRarely;


    public Article(String place, String text) {
        this.place = place;
        this.text = text;
    }
    public void extractArticle(List<Integer> attrib){

        for(Integer attr:attrib){
            switch (attr){
                case 1->{attributes.add(AttributesExtraction.countShortWords(text)); }
                case 2->{attributes.add(AttributesExtraction.countMediumWords(text));}
                case 3->{attributes.add(AttributesExtraction.countLongWords(text));}
                case 4->{this.metric=AttributesExtraction.metricUnitsInText(text);}
                case 5->{attributes.add(AttributesExtraction.countUppercaseWords(text));}
                case 6->{this.mostCommonWordFromRarely=AttributesExtraction.mostCommonWordFromRarely(text);}
                case 7->{attributes.add(AttributesExtraction.countPunctuationMarks(text));}
                case 8->{attributes.add(AttributesExtraction.digitsInText(text));}
                case 9->{this.currency=AttributesExtraction.checkCurrency(text);}
                case 10->{attributes.add(AttributesExtraction.countDistinctWords(text));}
            }
        }
    }

    public void setClassifiedPlace(String classifiedPlace) {
        this.classifiedPlace = classifiedPlace;
    }

    public List<Integer> getAttributes() {
        return attributes;
    }

    public String getClassifiedPlace() {
        return classifiedPlace;
    }

    public String getCurrency() {
        return currency;
    }

    public String getMetric() {
        return metric;
    }

    public String getMostCommonWordFromRarely() {
        return mostCommonWordFromRarely;
    }

    public String getPlace() {
        return place;
    }

    public String getText() {
        return text;
    }

    public boolean isLearning() {
        return isLearning;
    }

    public void setLearning(boolean learning) {
        isLearning = learning;
    }

    @Override
    public String toString() {
        return "Article{" +
                "place='" + place + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}

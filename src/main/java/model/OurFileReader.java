package model;

import model.Article;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Patryk Markowski
 */
public class OurFileReader {
    public OurFileReader() {
    }

    public ArrayList<Article> readAllFiles(String path){
        ArrayList<Article> articles = new ArrayList<>();
        List<String> textFiles = new ArrayList<String>();
        File dir = new File(path);
        for (File file : dir.listFiles()) {
            if (file.getName().endsWith((".sgm"))) {
                textFiles.add(file.getName());
                articles.addAll(readFile(path+"/"+file.getName()));
            }
        }

        return articles;
    }

    public ArrayList<Article> readFile(String path) {
        ArrayList<Article> articles = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;

            while ((line = br.readLine()) != null) {
                if (line.contains("<REUTERS")) {
                    String places = "";
                    String text = "";
                    boolean arePlaces = true;
                    while (!line.contains("</REUTERS>")) {
                        line = br.readLine();
                        // przypisanie PLACES (miejsca)
                        if (line.contains("<PLACES>")) {
                            places = line.replace("<PLACES>","").replace("</PLACES>","");
                            line = br.readLine();

                        }
                        // przypisanie BODY (tresci)
                        if (line.contains("<BODY>")) {
                            while (!line.contains("</BODY>")) {
                                text += line;
                                line = br.readLine();
                            }
                        }
                    }
                    Article article = new Article(cleanPlaces(places),cleanBody(text));
                    if (!article.getPlace().equals("") && !article.getText().equals("")) {

                        articles.add(article);
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return articles;
    }

   private String cleanPlaces(String places){

        if(places.length()==0)return "";
        places = places.replace("<D>"," ").replace("</D>","").substring(1);
        String[] arr = places.split(" ");
        if(arr.length>1)return "";

        return arr[0];
   }
       private static String cleanBody(String body){
        if(body.startsWith("<BODY>")) return body.replace("<BODY>","");
        String[] arr = body.split("<BODY>");
        if(arr.length==1)return arr[0];
            return arr[1] ;
       }



}

package model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Patryk Markowski
 */
public class Results {
   private List<Article> articles;
    private String[] countries = {"usa","japan","uk","france","west-germany","canada"};

    public Results(List<Article> articles) {

        this.articles = articles;
    }
    public int[][] mistakeTable(){
     int[][] arr = new int[6][6];
     for(int i=0;i<6;i++){
         for(int j=0;j<6;j++){
             arr[j][i] = countCounties(countries[i],countries[j]);
         }
     }
     return arr;
    }
    private int countCounties(String country1,String country2){
        int count = 0;
        count= (int) articles.stream().filter(article -> article.getClassifiedPlace().equals(country1))
                .filter(article -> article.getPlace().equals(country2)).count();
        if(Double.isNaN(count)) return 0;
        return count;
    }

//    public List<Integer> calc(String country) {
//                List<Integer> resultForUSA = new ArrayList<>();
//                resultForUSA.add(calcTruePositive(country));
//                resultForUSA.add(calcTrueNegative(country));
//                resultForUSA.add(calcFalsePositive(country));
//                resultForUSA.add(calcFalseNegative(country));
//        return resultForUSA;
//
//    }
    //filtrujemy wszystkie które należą do klasy (Place zgodne z country)
    //filtrujemy wszystkie które są zgodne (Place i ClassifiedPlace)
    private int calcTruePositive (String country){
        int count = 0;
        count= (int) articles.stream().filter(article ->article.getClassifiedPlace().equals(country))
                .filter(article -> article.getPlace().equals(country)).count();
        if(Double.isNaN(count)) return 0;
        return count;
    }
    //filtrujemy wszystkie które NIE zostały zaklasyfikowane jako country (ClassifiedPlace różne od country)
    //filtrujemy wszystkie które NIE są zgodne (Place i country)
    private int calcTrueNegative(String country){
        int count = 0;
        count= (int) articles.stream().filter(article -> !article.getClassifiedPlace().equals(country))
                .filter(article -> !article.getPlace().equals(country)).count();
        if(Double.isNaN(count)) return 0;
        return count;
    }
    //filtrujemy wszytkie które zostały rozpoznane jako country (ClassifiedPlace równe country)
    // Place i ClassifiedPlace NIE są zgodne
    private int calcFalsePositive(String country){
        int count = 0;
        count= (int) articles.stream().filter(article ->article.getClassifiedPlace().equals(country))
                .filter(article -> !article.getPlace().equals(country)).count();
        if(Double.isNaN(count)) return 0;
        return count;
    }
    //filtrujemy wszystkie które NIE zostały zaklasyfikowane jako country (ClassifiedPlace różne od country)
    //filtrujemy wszystkie które należą do country (Place równe country)
    private int calcFalseNegative(String country){
        int count = 0;
        count= (int) articles.stream().filter(article ->article.getPlace().equals(country))
                .filter(article -> !article.getClassifiedPlace().equals(country)).count();
        if(Double.isNaN(count)) return 0;
        return count;
    }


    public double calcPrecisionForOneCountry(String country){
        int TP = calcTruePositive(country);
        int FP = calcFalsePositive(country);
        double result = (double)TP/(TP+FP);
        if(Double.isNaN(result)) return 0;
        return result;
    }
    public double calcRecallForOneCountry(String country){
        int TP = calcTruePositive(country);
        int FN = calcFalseNegative(country);
        if(TP+FN==0 )return 0;
        double result = (double)TP/(TP+FN);
        if(Double.isNaN(result)) return 0;
        return result;
    }
    public double calcF1ForOneCountry(String country){
        double precision = calcPrecisionForOneCountry(country);
        double recall = calcRecallForOneCountry(country);

        if(precision+recall==0.0)return 0;
        return (2*precision*recall)/(precision+recall);
    }

    public double calculateAccuracy() {
        double TP = 0.0;
        double TN=0.0;
        double FP =0.0;
        double FN=0.0;
        for(int i=0;i<6;i++){
            TP+=calcTruePositive(countries[i]);
//            TN+= calcTrueNegative(countries[i]);
//            FP+=calcFalsePositive(countries[i]);
//            FN+=calcFalseNegative(countries[i]);
        }

//        return (double) (TP+TN)/(TP+TN+FP+FN);
        return (double) (TP)/articles.size();
    }

//    public double calculatePrecision() {
//        double TP = 0.0;
//        double FP =0.0;
//        for(int i=0;i<6;i++){
//            TP+=calcTruePositive(countries[i]);
//            FP+=calcFalsePositive(countries[i]);
//        }
//
//        return (double)TP/(TP+FP);
//    }

    public double calculatePrecision() {
        double TP = 0.0;
        double FP =0.0;
        double sum=0;
        for(int i=0;i<6;i++){
            if(Double.isNaN(calcPrecisionForOneCountry(countries[i]))) continue;
         sum+=calcPrecisionForOneCountry(countries[i]);
        }


        return sum/6;
    }
//    public double calculateRecall() {
//        double TP = 0.0;
//        double FN =0.0;
//        for(int i=0;i<6;i++){
//            TP+=calcTruePositive(countries[i]);
//            FN+=calcFalseNegative(countries[i]);
//        }
//
//        return (double)TP/(TP+FN);
//    }
public double calculateRecall() {
   double sum = 0;
    for(int i=0;i<6;i++){
        if(Double.isNaN(calcRecallForOneCountry(countries[i]))) continue;
    sum+=calcRecallForOneCountry(countries[i]);
    }

    return (double)sum/6;
}

    public double calculateF1() {

        double precision = calculatePrecision();
        double recall = calculateRecall();
        if(precision+recall==0.0)return 0;
        return (2*precision*recall)/(precision+recall);
    }
}

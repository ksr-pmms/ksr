package extraction;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Patryk Markowski
 */
public class AttributesExtraction {
    /*
    -krótkie słowa x<4
    -średnie 4<x<8
    -długie x>8
    -występowanie jednostek metrycznych
    -ilość słów zapisanych wielką literą
    -czy tekst jest napisany w stylu formalnym
    -ilość znaków interpuncyjnych
    -występowanie waluty w tekście
    -słowo występujące najczęściej ze zbioru 20 % najrzadziej występujących w tekście
    - ilość słów w tekście po usunięciu wyrazów powtarzających się
     */



    public static int countShortWords(String text) {
        String[] arr = text.replaceAll("\\p{P}", "").split(" ");
        return (int) Arrays.asList(arr).stream().filter(v -> v.length() <= 4).count();
    }

    public static int countMediumWords(String text) {
        String[] arr = text.replaceAll("\\p{P}", "").split(" ");
        return (int) Arrays.asList(arr).stream().filter(v -> v.length() > 4 && v.length() < 8).count();
    }

    public static int countLongWords(String text) {
        String[] arr = text.replaceAll("\\p{P}", "").split(" ");
        return (int) Arrays.asList(arr).stream().filter(v -> v.length() >= 8).count();
    }

    public static String metricUnitsInText(String text) {
        String[] possibleMetrics = {" miles "," km ", " kms "," kilometers "," kg "," pound "," inch "," ounce "};
        for (String s : possibleMetrics) {
            if (text.contains(s)) {
                return s;
            }
        }
        return "";
    }

    public static int countUppercaseWords(String text) {
        String[] arr = text.split(" ");
        return (int) Arrays.asList(arr).stream().filter(v -> {
            if(v.length()>0){
            if(Character.isUpperCase(v.charAt(0)))return true;
            }
            return false;
        }).count();

    }

    public static int digitsInText(String text) {
      return (int) text.chars()
              .filter(Character::isDigit)
              .count();
    }

    public static int countPunctuationMarks(String body) {
        String text = body;
        int len = text.length();
        int len2 = text.replaceAll("\\p{Punct}", "").length();
        return len - len2;
    }

    public static String checkCurrency(String text) {
        String[] possibleCurrency = {"dlrs", "dollar", "$", " cent ", "€", " euro ", " yen "};

        for (String s : possibleCurrency) {
            if (text.contains(s)) {
                return s;
            }
        }
        return "";
    }

    public static String mostCommonWordFromRarely(String text) {
        String[] arr = text.replaceAll("\\p{P}", "").split(" ");
        Map<String, Integer> map = new HashMap<>();
        for (String s : arr) {
            if (map.containsKey(s)) {
                map.put(s, map.get(s) + 1);
            } else {
                map.put(s, 1);
            }
        }
        List<Map.Entry<String, Integer>> sortedMap =
                map.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .collect(Collectors.toList());
        int value = (int) (sortedMap.size()*0.2);
        return sortedMap.get(value).getKey();
    }

    public static int countDistinctWords(String text) {
        String[] arr = text.replaceAll("\\p{P}", "").split(" ");
        return (int) Arrays.asList(arr).stream().distinct().count();
    }
}
